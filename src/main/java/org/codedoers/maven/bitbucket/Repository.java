/*
 * Copyright 2018 Codedoers.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.codedoers.maven.bitbucket;

public class Repository {

	public static String FIELDS = "pagelen,size,next,page,values.slug,values.name,values.links,values.owner";

	private String slug;
	private String name;
	private Links links;
	private Owner owner;

	public String getSlug() {
		return slug;
	}

	public String getName() {
		return name;
	}

	public Self getDownloads() {
		return links.getDownloads();
	}

	public Owner getOwner() {
		return owner;
	}

	public static class Page extends org.codedoers.maven.bitbucket.Page<Repository> {

	}

	public static class Links {

		private Self downloads;

		public Self getDownloads() {
			return downloads;
		}
	}

	@Override
	public String toString() {
		return "Repository{" + "slug=" + slug + ", name=" + name + ", downloads=" + getDownloads() + ", owner=" + owner + '}';
	}

}
