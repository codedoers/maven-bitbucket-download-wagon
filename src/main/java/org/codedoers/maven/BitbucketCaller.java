/*
 * Copyright 2018 Codedoers.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.codedoers.maven;

import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.maven.wagon.ResourceDoesNotExistException;
import org.apache.maven.wagon.TransferFailedException;
import org.apache.maven.wagon.authorization.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BitbucketCaller {

	private static final Logger log = LoggerFactory.getLogger(BitbucketCaller.class);
	private static final Set<Integer> OK_RESPONSES = Stream.of(200, 201, 202).collect(Collectors.toSet());
	private static final Set<Integer> AUTH_NOTOK_RESPONSES = Stream.of(401, 403, 405).collect(Collectors.toSet());
	private static final Set<Integer> RESOURCE_NOTOK_RESPONSES = Stream.of(404).collect(Collectors.toSet());

	private static Gson gson = new Gson();
	private final Executor executor;

	public BitbucketCaller(String user, String password) {
		this.executor = Executor.newInstance()
				.auth(new HttpHost(BitbucketAPI.HOST, 443, "https"), user, password)
				.authPreemptive(new HttpHost(BitbucketAPI.HOST, 443, "https"));
	}

	public CastableWrapper page(String url, String fields) {
		return internalPage(url + "?pagelen=100&fields="+fields);
	}

	private CastableWrapper internalPage(String url) {
		try {
			return asCastable(executeCallTimes(url, 3));
		} catch (IOException ex) {
			log.error("When trying page for " + url, ex);
			throw new RuntimeException(ex);
		}
	}

	private Content executeCallTimes(String url, int times) throws IOException {
		try {
			return executor.execute(Request.Get(url)).returnContent();
		} catch (IOException ex) {
			log.warn("Error when times left " + times, ex);
			if (times > 0) {
				return executeCallTimes(url, times - 1);
			}
			throw ex;
		}
	}

	public CastableWrapper nextPage(String url) {
		return internalPage(url);
	}

	private CastableWrapper asCastable(Content content) {
		return new CastableWrapper(content);
	}

	public void upload(File file, String url, String fileName)
			throws IOException, AuthorizationException, ResourceDoesNotExistException, TransferFailedException {
		HttpEntity entity = MultipartEntityBuilder.create()
				.addBinaryBody("files", file, ContentType.MULTIPART_FORM_DATA, fileName)
				.build();
		Request request = Request.Post(url)
				.body(entity);
		HttpResponse response = executor.execute(request).returnResponse();
		handle(response);
	}

	public void download(File file, String url) throws IOException {
		Response response = executor.execute(Request.Get(url));
		response.saveContent(file);
	}

	private void handle(
			HttpResponse response) throws AuthorizationException, ResourceDoesNotExistException, TransferFailedException {
		int status = response.getStatusLine().getStatusCode();
		if (OK_RESPONSES.contains(status)) {
			return;
		}
		if (AUTH_NOTOK_RESPONSES.contains(status)) {
			throw new AuthorizationException(response.getStatusLine().getReasonPhrase());
		}
		if (RESOURCE_NOTOK_RESPONSES.contains(status)) {
			throw new ResourceDoesNotExistException(response.getStatusLine().getReasonPhrase());
		}
		throw new TransferFailedException(response.getStatusLine().getReasonPhrase());
	}

	public static class CastableWrapper {

		public final Content content;

		public CastableWrapper(Content content) {
			this.content = content;
		}

		public Content getContent() {
			return content;
		}

		public <T> T as(Class<T> cl) {
			return gson.fromJson(new InputStreamReader(content.asStream()), cl);
		}
	}

}
